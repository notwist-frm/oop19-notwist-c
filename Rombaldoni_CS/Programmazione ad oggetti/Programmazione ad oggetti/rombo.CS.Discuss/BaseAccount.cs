﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Programmazione_ad_oggetti
{
    /*This is the base interface  will be used to do the other specific interface and class.*/

    interface BaseAccount
    {
        /*This method must return the Account ID of the operations.*/
        public int GetID();

        /*If is possible this method return the username, if not possible return null*/
        public String GetUserName();
    }
}
