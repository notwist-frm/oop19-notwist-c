﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Programmazione_ad_oggetti
{
    interface SpecificLikeAndDislike
    {
        /*This method return the topic where is possible put like and dislike.*/
        public String GetTopic();

        /*If possible this method retutn the comments where is possible put like and dislike, else return null*/
        public CommentsImplements GetComments();
    }
}
