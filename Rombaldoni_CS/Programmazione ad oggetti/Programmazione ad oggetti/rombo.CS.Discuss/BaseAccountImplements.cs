﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Programmazione_ad_oggetti 
{
    class BaseAccountImplements :BaseAccount
    {

        /*Fileds.*/
        private int ID;
        private String Name = null;

        /*Base Builder.*/

        public BaseAccountImplements(int ID)
        {
            this.ID = ID;
        }

        /*Advace builder.*/
        public BaseAccountImplements(int ID, String UserName)
        {
            this.ID = ID;
            this.Name = UserName;
        }


        /*Get methods.*/

        /*This method return the ID.*/
        public int GetID()
        {
            return this.ID;
        }

        /*If possible this method return the name, else return null.*/
        public String GetUserName()
        {
            if(this.Name != null)
            {
                return this.Name;
            }
            else
            {
                return null;
            }
        }

    }
}
