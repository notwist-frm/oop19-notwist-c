﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Programmazione_ad_oggetti
{
    internal class StrikeImplements : BaseAccountImplements, Strike
    {
        /*Fileds.*/
        private int strike;

        /*Base builder.*/
        public StrikeImplements(int ID) : base(ID)
        {
            this.strike = 0;
        }

        /*Advace builder.*/
        public StrikeImplements(int ID, string UserName) : base(ID, UserName)
        {
            this.strike = 0;
        }

        /*Method for add a strike.*/
        public void ADDStrike()
        {
            this.strike++;
        }

        /*Method for remove a strike.*/
        public void RemoveStrike()
        {
            this.strike--;
        }

        /*Method for remove all strike.*/
        public void RemoveAllStrike()
        {
            this.strike = 0;
        }

        /*Method that return the number of the strikes.*/
        public int GetNumberOfStrikes()
        {
            return this.strike;
        }
               
    }
}
