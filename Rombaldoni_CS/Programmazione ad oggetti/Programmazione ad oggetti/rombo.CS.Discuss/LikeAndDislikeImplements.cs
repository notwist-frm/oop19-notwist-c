﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Programmazione_ad_oggetti
{
    class LikeAndDislikeImplements : LikeAndDislike
    {
        /*Fields.*/
        private static int LIKE = 1;
        private static int DISLIKE = -1;

        private int NumberLike;
        private int NumberDisLike;

        Dictionary<BaseAccountImplements, int> User = new Dictionary< BaseAccountImplements, int>();

        /*Builder.*/
        public LikeAndDislikeImplements()
        {
            this.NumberDisLike = 0;
            this.NumberLike = 0;
        }

        public void ADDDislike(BaseAccountImplements ID)
        {
            /*Control if this is a new user.*/
            if (User.ContainsKey(ID))
            {
                /*Control if the user made other action.*/
                int temp;
                User.TryGetValue(ID, out temp);

                if (temp == LIKE)
                {
                    this.NumberLike--;
                    this.NumberDisLike++;
                }
                else
                {
                    if (temp == DISLIKE)
                    {
                        this.NumberDisLike--;
                        User.Remove(ID);
                    }
                }
            }
            /*If this is a new user*/
            else
            {
                User.Add(ID, DISLIKE);
                this.NumberDisLike++;
            }
        }

        public void ADDLike(BaseAccountImplements ID)
        {
            /*Control if this is a new user.*/
            if (User.ContainsKey(ID))
            {
                /*Control if the user made other action.*/
                int temp;
                User.TryGetValue(ID, out temp);
                if (temp == DISLIKE)
                {
                    this.NumberDisLike--;
                    this.NumberLike++;
                }
                else
                {
                    if (temp == LIKE)
                    {
                        User.Remove(ID);
                        this.NumberLike--;
                    }
                }
            }
            /*If this is a new user*/
            else
            {
                User.Add(ID, LIKE);
                this.NumberLike++;
            }
        }

        public int GetDislikeNumber()
        {
            return this.NumberDisLike;
        }

        public int GetLikeNumber()
        {
            return this.NumberLike;
        }
    }
}
