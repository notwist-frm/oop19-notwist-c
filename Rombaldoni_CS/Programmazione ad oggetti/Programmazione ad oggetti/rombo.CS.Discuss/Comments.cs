﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Programmazione_ad_oggetti
{
    /*This interface extend "BaseAccount" that represent the base information of every users.*/
    interface Comments : BaseAccount
    {
        /*This method must return the comment.*/
        public String GetComment();

        /*If possible this method return the name of the topic where the comments was write, else return null*/
        public String GetTopic();

        /*If possible this method return the number of the comment, else return -1*/
        public int GetNumberOfComment();
    }
}
