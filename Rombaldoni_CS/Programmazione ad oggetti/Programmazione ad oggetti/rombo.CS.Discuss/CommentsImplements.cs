﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Programmazione_ad_oggetti
{
    internal class CommentsImplements : BaseAccountImplements,Comments
    {
        /*Fileds.*/
        private String Comment;
        private String Topic = null;
        private int Number =-1;

        /*Base builder*/
        public CommentsImplements(int ID, String Comment) : base(ID)
        {
            this.Comment = Comment;

        }

        /*Advance builders.*/
        /*(1)*/
        public CommentsImplements(int ID, String UserName,String Comment) : base(ID, UserName)
        {
            this.Comment = Comment;

        }

        /*(2)*/
        public CommentsImplements(int ID, String UserName, String Comment, String Topic) : base(ID, UserName)
        {
            this.Comment = Comment;
            this.Topic = Topic;
        }

        /*(3)*/
        public CommentsImplements(int ID, String UserName, String Comment, String Topic, int NumberOfComment) : base(ID, UserName)
        {
            this.Comment = Comment;
            this.Topic = Topic;
            this.Number = NumberOfComment;
        }


        public string GetComment()
        {
            return this.Comment;
        }

        public int GetNumberOfComment()
        {
            if (this.Number != 0)
            {
                return this.Number;
            }
            else
            {
                return -1;
            }
        }

        public string GetTopic()
        {
            if (this.Topic != null)
            {
                return this.Topic;
            }
            else
            {
                return null;
            }
        }
    }
}
