﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Programmazione_ad_oggetti
{
    class SpecificLikeAndDislikeImplements : LikeAndDislikeImplements, SpecificLikeAndDislike
    {
        /*Fileds.*/
        private String Topic;
        private CommentsImplements Comment = null;

        /*Base builder.*/
        public SpecificLikeAndDislikeImplements(String Topic): base()
        {
            this.Topic = Topic;
        }

        /*Advance builder.*/
        public SpecificLikeAndDislikeImplements(String Topic, CommentsImplements Comment ) : base()
        {
            this.Topic = Topic;
            this.Comment = Comment;
        }

        /*If possible this method return the comment, else return null.*/
        public CommentsImplements GetComments()
        {
            if (this.Comment!=null)
            {
                return this.Comment;
            }
            else
            {
                return null;
            }
        }

        /*This method return the topic whwre is possible put like and dislike.*/
        public String GetTopic()
        {
            throw new NotImplementedException();
        }
    }
}
