﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Programmazione_ad_oggetti
{
    interface Strike : BaseAccount
    {
        /*This method permit to add a strike for the user.*/
        public void ADDStrike();

        /*This method permit to remove a strike for the user.*/
        public void RemoveStrike();

        /*This method permit to remove all strike for the user.*/
        public void RemoveAllStrike();

        /*This method return the total number of strikes collected from the user.*/
        public int GetNumberOfStrikes();
    }
}
