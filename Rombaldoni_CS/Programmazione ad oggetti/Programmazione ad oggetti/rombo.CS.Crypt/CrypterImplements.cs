﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Programmazione_ad_oggetti.rombo.CS.Crypt
{
    class CrypterImplements :Crypter
    {
        /*Fields.*/
        private static char[] map1 = new char[64];
        private static sbyte[] map2 = new sbyte[128];


        /*Builder.*/
        public CrypterImplements() 
        {
            /*Creation of a first container of characters for code and encode the string.*/
            int i = 0;
            for (char c = 'A'; c <= 'Z'; c++) map1[i++] = c;
            for (char c = 'a'; c <= 'z'; c++) map1[i++] = c;
            for (char c = '0'; c <= '9'; c++) map1[i++] = c;
            map1[i++] = '+'; map1[i++] = '/';

            /*Creation of a second container of characters for code and encode the string.*/
            for (int j = 0; j < map2.Length; j++) map2[j] = -1;
            for (int j = 0; j < 64; j++) map2[map1[j]] = (sbyte)j;
        }



        /*Firts method.*/

           /*Method for encode a String.*/
	   
	   /*Encodes a byte array into Base64 format.
	    No blanks or line breaks are inserted in the output.
	    
	    @Param in    An array containing the data bytes to be encoded.
	    @Param iOff  Offset of the first byte in <code>in</code> to be processed.
	    @Param iLen  Number of bytes to process in <code>in</code>, starting at <code>iOff</code>.
	    Return      A character array containing the Base64 encoded data.
	    
	    This method permit of encode a byte array(I used an byte array because is more simple to do the conversion).*/
        private static char[] encode(sbyte[] inp, int iOff, int iLen)
        {
            /*Local fields*/
            int oDataLen = (iLen * 4 + 2) / 3; // output length without padding.
            int oLen = ((iLen + 2) / 3) * 4; // output length including padding.
            char[] outp = new char[oLen];
            int ip = iOff;
            int iEnd = iOff + iLen;
            int op = 0;

            /*Process of encode*/
            while(ip < iEnd)
            {
                int i0 = inp[ip++] & 0xff;
                int i1 = ip < iEnd ? inp[ip++] & 0xff : 0;
                int i2 = ip < iEnd ? inp[ip++] & 0xff : 0;
                int o0 = i0 >> 2;
                int o1 = ((i0 & 3) << 4) | (i1 >> 4);
                int o2 = ((i1 & 0xf) << 2) | (i2 >> 6);
                int o3 = i2 & 0x3F;
                outp[op++] = map1[o0];
                outp[op++] = map1[o1];
                outp[op] = op<oDataLen? map1[o2] : '='; op++;
                outp[op] = op<oDataLen? map1[o3] : '='; op++;
            }
            return outp;
        }

        /*Second method.*/

        /*Encodes a byte array into Base64 format.
         No blanks or line breaks are inserted in the output.

           @Param in  An array containing the data bytes to be encoded.
           @Return    A character array containing the Base64 encoded data.

        This method permit to convert a string into a byte array.*/

        private static char[] ToArrayEn(sbyte[] inp)
        {
            return encode(inp, 0, inp.Length);
        }


        /*Third method (this is the method for encode a string).*/

        /*Encodes a string into Base64 format.
        No blanks or line breaks are inserted.

        @Param s  A String to be encoded.
        @Return   A String containing the Base64 encoded data.*/
        public String Crypt(String s)
        {
            return new String(ToArrayEn((sbyte[])(Array)Encoding.Unicode.GetBytes(s)));
        }

        /*Dedicated part of class for decode a entry string*/

        /*Fourth method.*/
        /*Dedicated part of class for decode a entry string and after exit a decode string.*/

        /* Decodes a byte array from Base64 format.
         No blanks or line breaks are allowed within the Base64 encoded input data.

        @Param in    A character array containing the Base64 encoded data.
        @Param iOff  Offset of the first character in <code>in</code> to be processed.
        @Param iLen  Number of characters to process in <code>in</code>, starting at <code>iOff</code>.
        @Return      An array containing the decoded data bytes.

        Throws      IllegalArgumentException If the input is not valid Base64 encoded data.(in this specific case this is unnecessary)*/
        private static sbyte[] decode(char[] inp, int iOff, int iLen)
        {
            if (iLen % 4 != 0) throw new System.ArgumentException("Length of Base64 encoded input string is not a multiple of 4.");

            /*Part of decode.*/
            while (iLen > 0 && inp[iOff+iLen-1] == '=') iLen--;
            int oLen = (iLen * 3) / 4;
            sbyte[] outp = new sbyte[oLen];
            int ip = iOff;
            int iEnd = iOff + iLen;
            int op = 0;

            while (ip < iEnd)
            {
                int i0 = inp[ip++];
                int i1 = inp[ip++];
                int i2 = ip < iEnd ? inp[ip++] : 'A';
                int i3 = ip < iEnd ? inp[ip++] : 'A';
                if (i0 > 127 || i1 > 127 || i2 > 127 || i3 > 127)
                {
                    throw new System.ArgumentException("Illegal character in Base64 encoded data.");
                }
                int b0 = map2[i0];
                int b1 = map2[i1];
                int b2 = map2[i2];
                int b3 = map2[i3];

                if (b0 < 0 || b1 < 0 || b2 < 0 || b3 < 0)
                {
                    throw new System.ArgumentException("Illegal character in Base64 encoded data.");
                }

                int o0 = (b0 << 2) | (b1 >> 4);
                int o1 = ((b1 & 0xf) << 4) | (b2 >> 2);
                int o2 = ((b2 & 3) << 6) | b3;
                outp[op++] = (sbyte)o0;

                if (op < oLen) outp[op++] = (sbyte)o1;
                if (op < oLen) outp[op++] = (sbyte)o2;
            }
            return outp;
        }

        /*Fiveth method.*/
        /*Decodes a byte array from Base64 format.
	   No blanks or line breaks are allowed within the Base64 encoded input data.
	   
	   @Param in  A character array containing the Base64 encoded data.
	   @Return    An array containing the decoded data bytes.
	   
	   Throws    IllegalArgumentException If the input is not valid Base64 encoded data.*/
        private static sbyte[] ArrayPropeties(char[] inp)
        {
            return decode(inp, 0, inp.Length);
        }

        /*Sixth method.*/

        /*Decodes a byte array from Base64 format.
	   No blanks or line breaks are allowed within the Base64 encoded input data.
	   
	   @Param s  A Base64 String to be decoded.
	   @Return   An array containing the decoded data bytes.
	   
	   Throws   IllegalArgumentException If the input is not valid Base64 encoded data.*/
        private static sbyte[] ToArrayDe(String s)
        {
            return ArrayPropeties(s.ToCharArray());
        }

        /*Seventh method (this is the method for decode a string)*/
        /*Decodes a string from Base64 format.
	   No blanks or line breaks are allowed within the Base64 encoded input data.
	   
	   @Param s  A Base64 String to be decoded.
	   @Return   A String containing the decoded data.
	   
	   Throws   IllegalArgumentException If the input is not valid Base64 encoded data.*/
        public String Decrypt(String s)
        {
            sbyte[] temp1 = ToArrayDe(s);
            String temp2 = Convert.ToString(temp1);
            return temp2;
        }
    }
}
