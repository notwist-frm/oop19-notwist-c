﻿using System;
using System.Windows.Forms;

namespace MyProject
{
    static class Program
    {
        /// <summary>
        /// Main of the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Gui());
        }
    }
}
