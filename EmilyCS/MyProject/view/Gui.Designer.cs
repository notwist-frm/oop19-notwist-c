﻿using MyProject.view;

namespace MyProject
{
    partial class Gui
    {
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean resources
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.logoPan = new MyProject.view.LogoPan();
            this.upperPan = new MyProject.view.UpperPan();
            this.loginPan = new MyProject.Login();

            this.bodyHolder = new System.Windows.Forms.TabControl();
            this.loginHolder = new System.Windows.Forms.TabPage();

            this.bodyHolder.SuspendLayout();
            this.loginHolder.SuspendLayout();

            this.SuspendLayout();
            // 
            // Calling Logo panel.
            // 
            this.logoPan.Location = new System.Drawing.Point(0, 0);
            this.logoPan.Name = "Logo Panel";
            this.logoPan.Size = new System.Drawing.Size(504, 438);
            this.logoPan.TabIndex = 0;
            // 
            // Calling Close panel.
            // 
            this.upperPan.Location = new System.Drawing.Point(592, 0);
            this.upperPan.Name = "Close Panel";
            this.upperPan.Size = new System.Drawing.Size(288, 40);
            this.upperPan.TabIndex = 1;
            // 
            // Calling the Login panel before adding it to the Tab Page.
            // 
            this.loginPan.Location = new System.Drawing.Point(0, 0);
            this.loginPan.Name = "Login Panel";
            this.loginPan.Size = new System.Drawing.Size(370, 392);
            this.loginPan.TabIndex = 1;
            // 
            // There is no cardLayout in C#, so I had to get creative. I am using a Table Control and exploiting a
            // "dirty trick" to hide the pages' names. 
            // 
            this.bodyHolder.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.bodyHolder.ItemSize = new System.Drawing.Size(0, 1);
            this.bodyHolder.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;

            this.bodyHolder.Controls.Add(this.loginHolder);
            this.bodyHolder.Location = new System.Drawing.Point(510, 46);
            this.bodyHolder.Name = "tabControl1";
            this.bodyHolder.SelectedIndex = 1;
            this.bodyHolder.Size = new System.Drawing.Size(362, 383);
            this.bodyHolder.TabIndex = 1;
            // 
            // Adding the login to the Tab Page.
            // 
            this.loginHolder.Controls.Add(this.loginPan);
            this.loginHolder.Location = new System.Drawing.Point(4, 5);
            this.loginHolder.Name = "Login";
            this.loginHolder.Padding = new System.Windows.Forms.Padding(3);
            this.loginHolder.Size = new System.Drawing.Size(354, 374);
            this.loginHolder.TabIndex = 0;
            this.loginHolder.UseVisualStyleBackColor = true;
            // 
            // Adding all components.
            // 
            this.ClientSize = new System.Drawing.Size(883, 441);
            this.Controls.Add(this.bodyHolder);
            this.Controls.Add(this.logoPan);
            this.Controls.Add(this.upperPan);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Gui";
            this.bodyHolder.ResumeLayout(false);
            this.loginHolder.ResumeLayout(false);
            this.ResumeLayout(false);
            //
            // Method to move borderless Form. Doesn't work as well as Java method, but still the best
            // I could think of.
            //
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gui_MouseDown);

        }
        private UpperPan upperPan;
        private LogoPan logoPan;
        private Login loginPan;
        private System.Windows.Forms.TabControl bodyHolder;
        private System.Windows.Forms.TabPage loginHolder;
    }
}

