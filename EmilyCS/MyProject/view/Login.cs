﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace MyProject
{
    class Login : Panel
    {
        public Login()
        {
            DrawLogin();
        }
        private void DrawLogin()
        {
            this.loginPan = new Panel();
            this.signUpLink = new Label();
            this.loginBtn = new Button();
            this.sep1 = new Label();
            this.psswField = new TextBox();
            this.psswLabel = new Label();
            this.sep2 = new Label();
            this.mailText = new TextBox();
            this.mailLabel = new Label();
            this.loginPan.SuspendLayout();
            this.SuspendLayout();
            // 
            // Adding the controls to the panel.
            // 
            this.loginPan.Controls.Add(this.signUpLink);
            this.loginPan.Controls.Add(this.loginBtn);
            this.loginPan.Controls.Add(this.sep1);
            this.loginPan.Controls.Add(this.psswField);
            this.loginPan.Controls.Add(this.psswLabel);
            this.loginPan.Controls.Add(this.sep2);
            this.loginPan.Controls.Add(this.mailText);
            this.loginPan.Controls.Add(this.mailLabel);
            this.loginPan.Name = "Login";
            this.loginPan.Size = new Size(370, 480);
            this.loginPan.TabIndex = 0;
            // 
            // Drawing the label to connect Register to Login. I did not implemented it but it is supposed to handle a "clicked" event
            // and change the tablepage.
            // 
            this.signUpLink.AutoSize = true;
            this.signUpLink.Location = new Point(58, 276);
            this.signUpLink.Name = "label5";
            this.signUpLink.Size = new Size(174, 13);
            this.signUpLink.TabIndex = 7;
            this.signUpLink.Text = "Non hai un account? Registrati qui.";
            // 
            // Drawing the login button.
            // 
            this.loginBtn.Location = new Point(61, 215);
            this.loginBtn.Name = "button1";
            this.loginBtn.Size = new  Size(255, 58);
            this.loginBtn.TabIndex = 6;
            this.loginBtn.Text = "LOGIN";
            this.loginBtn.UseVisualStyleBackColor = true;
            // 
            // Since there is no JSeparator in C#, I am drawing a label, setting its border to 3D and 
            // shrinking it to show only the border.
            // 
            this.sep1.BorderStyle = BorderStyle.FixedSingle;
            this.sep1.Location = new Point(61, 186);
            this.sep1.Name = "Separator";
            this.sep1.Size = new Size(255, 2);
            this.sep1.TabIndex = 5;
            // 
            // Since there is no JPasswordField in C#, I am using a Textbox and set a password char
            // that is going to hide the user's input.
            // 
            this.psswField.Location = new Point(61, 163);
            this.psswField.Name = "Password Field";
            this.psswField.PasswordChar = '*';
            this.psswField.Size = new Size(255, 20);
            this.psswField.TabIndex = 4;
            this.psswField.GotFocus += PasswordOnFocus;
            // 
            // Drawing the password label.
            // 
            this.psswLabel.AutoSize = true;
            this.psswLabel.Location = new Point(58, 136);
            this.psswLabel.Name = "psswLabel";
            this.psswLabel.Size = new  Size(53, 13);
            this.psswLabel.TabIndex = 3;
            this.psswLabel.Text = "Password";
            // 
            // Since there is no JSeparator in C#, I am drawing a label, setting its border to 3D and 
            // shrinking it to show only the border.
            // 
            this.sep2.BorderStyle = BorderStyle.FixedSingle;
            this.sep2.Location = new Point(61, 110);
            this.sep2.Name = "Separator";
            this.sep2.Size = new  Size(255, 2);
            this.sep2.TabIndex = 2;
            // 
            // Drawing the mail Textfield.
            // 
            this.mailText.Location = new Point(61, 87);
            this.mailText.Name = "mailText";
            this.mailText.Size = new Size(255, 20);
            this.mailText.TabIndex = 1;
            this.mailText.GotFocus += MailOnFocus;
            // 
            // Drawing the mail Label.
            // 
            this.mailLabel.AutoSize = true;
            this.mailLabel.Location = new Point(58, 60);
            this.mailLabel.Name = "mailLabel";
            this.mailLabel.Size = new Size(26, 13);
            this.mailLabel.TabIndex = 0;
            this.mailLabel.Text = "Mail";
            // 
            // Drawing the Login panel
            // 
            this.Controls.Add(this.loginPan);
        }

        private void PasswordOnFocus(object sender, EventArgs e)
        {
            this.psswLabel.ForeColor = Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(175)))), ((int)(((byte)(218)))));
            this.mailLabel.ForeColor = Color.Black;
        }
        private void MailOnFocus(object sender, EventArgs e)
        {
            this.mailLabel.ForeColor = Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(175)))), ((int)(((byte)(218)))));
            this.psswLabel.ForeColor = Color.Black;
        }

        private Panel loginPan;
        private Label signUpLink;
        private Button loginBtn;
        private Label sep1;
        private TextBox psswField;
        private Label psswLabel;
        private Label sep2;
        private TextBox mailText;
        private Label mailLabel;
    }
}
