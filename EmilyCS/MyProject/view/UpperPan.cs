﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace MyProject.view
{
    class UpperPan : Panel
    {
        public UpperPan()
        {
            DrawUpperPan();
        }

        private void DrawUpperPan()
        {
            this.upperPanel = new Panel();
            this.close = new Label();

            this.upperPanel.Name = "Exit Panel";
            this.upperPanel.Size = new Size(288, 40);
            this.upperPanel.TabIndex = 0;
            // 
            // Drawing a label and setting its image to proper icon.
            // 
            this.close.Image = global::EmilyCS.Properties.Resources.close;
            this.close.Location = new Point(253, 7);
            this.close.MinimumSize = new Size(25, 25);
            this.close.Size = new Size(25, 25);
            this.close.TabIndex = 1;
            this.close.Click += new System.EventHandler(this.close_Click);

            this.upperPanel.Controls.Add(this.close);
            this.Controls.Add(this.upperPanel);

        }
        //
        // Close method for the borderless Form.
        //
        private void close_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private Panel upperPanel;
        private Label close;
    }
}
