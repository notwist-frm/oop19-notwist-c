﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace MyProject
{
    public partial class Gui : Form
    {
        public Gui()
        {
            InitializeComponent();
        }
        //
        // Building the methods to move my borderless Form.
        // I am going to use ReleaseCapture to remove mouse capture from the object in the current document and SendMessage to send 
        // the specified message to a window. It calls the window procedure for the specified window and does not return until the 
        // window procedure has processed the message.
        //
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        private void gui_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }
    }
}
