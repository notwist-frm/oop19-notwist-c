﻿using System.Drawing;
using System.Windows.Forms;

namespace MyProject.view
{
    class LogoPan : Panel
    {
        public LogoPan()
        {
            DrawLogoPan();
        }
        private void DrawLogoPan()
        {
            this.logoPan = new Panel();
            // 
            // Since the Logo image is smaller than its cointainer, I am setting the same backgroundcolor as the image
            // to hide this problem.
            // 
            this.logoPan.BackColor = Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(71)))), ((int)(((byte)(91)))));
            this.logoPan.BackgroundImage = global::EmilyCS.Properties.Resources.font_test_1;
            this.logoPan.BackgroundImageLayout = ImageLayout.Center;
            this.logoPan.Margin = new Padding(0);
            this.logoPan.Name = "Logo Panel";
            this.logoPan.Size = new Size(510, 444);
            this.logoPan.TabIndex = 5;

            this.Controls.Add(this.logoPan);
        }
        private Panel logoPan;
    }
}
