﻿namespace Messina_CS
{
    public class DiscussionImpl
    {
        public int IdDiscussion {get;}
        public int IdUser { get; }
        public string Title { get; }
        public string Description { get; }
        public Category Category { get; }
        public System.DateTime Date { get; }

        public DiscussionImpl(int idDiscussion, int idUser, string title, string description, Category category, System.DateTime date)
        {
            this.IdDiscussion = idDiscussion;
            this.IdUser = idUser;
            this.Title = title;
            this.Description = description;
            this.Category = category;
            this.Date = date;
        }


    }
}