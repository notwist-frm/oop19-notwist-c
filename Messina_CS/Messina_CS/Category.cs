﻿namespace Messina_CS
{
    public class Category
    {
        public int IdCategory { get; }
        public string Name { get; }

        public Category(int IdCategory, string Name)
        {
            this.IdCategory = IdCategory;
            this.Name = Name;
        }
    }
}