﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace Messina_CS
{
    public class DiscussionImplDB : DiscussionDB
    {
        readonly LikeDislikeImplDB dblike = new LikeDislikeImplDB();
        private const int MAX_TOP = 5;

        public List<DiscussionImpl> GetAll()
        {
            return base.Read();
        }

        public Boolean CreateDiscussion(int idUser, String title, String description,
            Category category)
        {
          return base.Create(new DiscussionImpl(0, idUser, title, description, category, new DateTime().Date));
        }

        public DiscussionImpl GetDiscussion(int idDiscussion)
        {
            return base.Read().Find(d => d.IdDiscussion == idDiscussion);
        }

        public DiscussionImpl GetDiscussion(string title)
        {
            return base.Read().Find(d => d.Title.Contains(title));
        }

        public DiscussionImpl GetDiscussion(Category cat)
        {
            return base.Read().Find(d => d.Category.IdCategory == cat.IdCategory);
        }


        public List<DiscussionImpl> GetTopDiscussion()
        {
            List<DiscussionImpl> list = base.Read();
            list.Sort((a, b) => (dblike.GetLikes(b.IdDiscussion) - dblike.GetDislikes(b.IdDiscussion))
                                              .CompareTo(dblike.GetLikes(a.IdDiscussion) - dblike.GetDislikes(a.IdDiscussion)));
            return list;
        }



    }
}
