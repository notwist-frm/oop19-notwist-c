﻿using System;
namespace Messina_CS
{
    public class User
    {
        public int IdUser { get;}
        public string Username { get; }
        public string Email { get;  }
        public string Password { get; }
        public Boolean IsModerator { get;}

        public User(int idUser, string username, string email, string password, Boolean isModerator)
        {
            this.IdUser = idUser;
            this.Username = username;
            this.Email = email;
            this.Password = password;
            this.IsModerator = isModerator;
        }

        public override String ToString()
        {
            return $"User: {Username}\nIdUser: {IdUser}\nEmail: {Email}\nPassword: {Password}\nIsModerator: { IsModerator}";
        }
    }
}
