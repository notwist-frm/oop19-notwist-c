﻿namespace Messina_CS
{
    public class LikeSet
    {
        public int Id { get; }
        public bool Like { get; }
        public bool Dislike { get; }
        public int IdUser { get; }
        public int IdDiscussion { get; }
        public int IdComment { get; }

        public LikeSet(int id, bool like, bool dislike, int idUser, int idDiscussion, int idComment)
        {
            this.Id = id;
            this.Like = like;
            this.Dislike = dislike;
            this.IdUser = idUser;
            this.IdDiscussion = idDiscussion;
            this.IdComment = idComment;
        }
    }
}