﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace Messina_CS
{
    public class LikeDislikeDB : ManagerImplDB
    {
        public LikeDislikeDB()
        { }
        private string query;
        private MySqlDataReader dataReader;
        private MySqlCommand cmd;
        private const int MAX_TOP = 5;

        public List<LikeSet> Read()
        {
            List<LikeSet> list = new List<LikeSet>();
            query = "SELECT * FROM LIKES";


            if (this.Open())
            {
                cmd = new MySqlCommand(query, this.Connection);
                dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    list.Add(new LikeSet((int)dataReader["idLikes"], (bool)dataReader["isLike"],
                                            (bool)dataReader["isDislike"], (int)dataReader["idUser"],
                                             (int)dataReader["idDiscussion"], (int)dataReader["idComment"]));
                }
                dataReader.Close();
                this.Close();

            }
            return list;
        }

        public Boolean Create(LikeSet likeSet)
        {
            query = $"insert into LIKES(idUser, idDiscussion, isLike, isDislike) values({likeSet.IdUser},'{likeSet.IdDiscussion}',{likeSet.Like}','{likeSet.Dislike}')'";

            if (this.Open())
            {
                cmd = new MySqlCommand(query, this.Connection);

                cmd.ExecuteNonQuery();
                this.Close();
                return true;
            }
            return false;
        }

        public Boolean Update(LikeSet likeSet)
        {
            query = $"update LIKES set isLike= {likeSet.Like}, isDislike= {likeSet.Dislike} where idLikes= {likeSet.Id}";

            if (this.Open())
            {
                cmd = new MySqlCommand(query, this.Connection);

                cmd.ExecuteNonQuery();
                this.Close();
                return true;
            }
            return false;
        }

        public Boolean Delete(int idLikes)
        {
            query = $"DELETE FROM LIKES WHERE idLikes= {idLikes}";

            if (this.Open())
            {
                cmd = new MySqlCommand(query, this.Connection);

                cmd.ExecuteNonQuery();
                this.Close();
                return true;
            }
            return false;
        }
    }
}
