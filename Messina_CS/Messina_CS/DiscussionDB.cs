﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace Messina_CS
{
    public class DiscussionDB : ManagerImplDB
    {
        private string query;
        private MySqlDataReader dataReader;
        private MySqlCommand cmd;
        private const int MAX_TOP = 5;

        public List<DiscussionImpl> Read()
        {
            List<DiscussionImpl> list = new List<DiscussionImpl>();
            query = "SELECT * FROM DISCUSSION";


            if (this.Open())
            {
                cmd = new MySqlCommand(query, this.Connection);
                dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                                                                                                                                                                                                         //will be CategoryDB.getCategory(String dataReader["idMacro"])                   
                    list.Add(new DiscussionImpl(int.Parse((string)dataReader["idDiscussion"]), int.Parse((string)dataReader["idUser"]), (string)dataReader["title"], (string)dataReader["description"], (Category)dataReader["idMacro"], (DateTime)dataReader["data"]));
                }
                dataReader.Close();
                this.Close();

            }
            return list;
        }

        public Boolean Create(DiscussionImpl discussion)
        {
            DateTime date = new DateTime();
            query = $"INSERT INTO DISCUSSION (idUser, title,description, idMacro, data) VALUES({discussion.IdUser},'{discussion.Title}',{discussion.Description}','{discussion.Category.IdCategory}','{date.Date.ToString("yyyyMMdd")}'";

            if (this.Open())
            {
                cmd = new MySqlCommand(query, this.Connection);

                cmd.ExecuteNonQuery();
                this.Close();
                return true;
            }
            return false;
        }

        public Boolean Update(DiscussionImpl disc)
        {
            query = $"INSERT INTO DISCUSSION (title,description, idMacro) VALUES({disc.Title},{disc.Description}','{disc.Category.IdCategory}')";

            if (this.Open())
            {
                cmd = new MySqlCommand(query, this.Connection);

                cmd.ExecuteNonQuery();
                this.Close();
                return true;
            }
            return false;
        }

        public Boolean Delete(int idDiscussion)
        {
            query = $"DELETE FROM DISCUSSION WHERE idDiscussion= {idDiscussion}";

            if (this.Open())
            {
                cmd = new MySqlCommand(query, this.Connection);

                cmd.ExecuteNonQuery();
                this.Close();
                return true;
            }
            return false;
        }

    }

}
