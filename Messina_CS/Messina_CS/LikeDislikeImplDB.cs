﻿using System;
namespace Messina_CS
{
    public class LikeDislikeImplDB
    {
        readonly LikeDislikeDB Dblike = new LikeDislikeDB();

        public int GetLikes(int idDiscussion)
        {
            return Dblike.Read().FindAll(l => l.Like && l.IdDiscussion == idDiscussion).Count;
        }

        public int GetDislikes(int idDiscussion)
        {
            return Dblike.Read().FindAll(l => l.Dislike && l.IdDiscussion == idDiscussion).Count;
        }


    }
}
